<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel 7 Socialite Login with Google Gmail Account
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-socialite-login-with-google-gmail-account.git`
2. Go inside the folder: `cd laravel-socialite-login-with-google-gmail-account`
3. Run `cp .env.example .env`. Then create database and put your DB credential.
4. Set your Google account https://console.developers.google.com/
5. Run `php artisan migrate`
6. Run `php artisan serve`

### Screen shot
Login Page

![Login Page](img/login.png "Login Page")

Google Page

![Google Page](img/google.png "Google Page")

Success Login

![Success Login](img/success.png "Success Login")

